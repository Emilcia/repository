package unitofwork;


import java.util.LinkedHashMap;
import java.util.Map;

import org.hibernate.SessionFactory;

import domain.Entity;
import domain.EntityState;

public class UnitOfWork implements IUnitOfWork{

	
	private SessionFactory session;
	
	private Map<Entity, IUnitOfWorkRepository> entities = 
			new LinkedHashMap<Entity, IUnitOfWorkRepository>();
	
	public UnitOfWork(SessionFactory session) {
		super();
		this.session = session;
		
	}

	
	public void commit() {

		for(Entity entity: entities.keySet())
		{
			switch(entity.getState())
			{
			case Changed:
				entities.get(entity).persistUpdate(entity);
				break;
			case Deleted:
				entities.get(entity).persistDelete(entity);
				break;
			case New:
				entities.get(entity).persistAdd(entity);
				break;
			case Unchanged:
				break;
			default:
				break;}
		}
		
	
			session.getCurrentSession().getTransaction().commit();
			entities.clear();
		
	}

	
	public void rollback() {

		entities.clear();
		
	}

	
	public void markAsNew(Entity entity, IUnitOfWorkRepository repository) {
		entity.setState(EntityState.New);
		entities.put(entity, repository);
		
	}

	
	public void markAsDirty(Entity entity, IUnitOfWorkRepository repository) {
		entity.setState(EntityState.Changed);
		entities.put(entity, repository);
		
	}

	
	public void markAsDeleted(Entity entity, IUnitOfWorkRepository repository) {
		entity.setState(EntityState.Deleted);
		entities.put(entity, repository);
		
	}

}
