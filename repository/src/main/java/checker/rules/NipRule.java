package checker.rules;

import checker.CheckResult;
import checker.ICanCheckRule;
import checker.RuleResult;
import domain.Person;

public class NipRule implements ICanCheckRule<Person> {
	
	
	
	   public static boolean isValidNip(String nip) {
	        nip = nip.replaceAll("-", "");
	        int nsize = nip.length();
	        if (nsize != 10) {
	            return false;
	        }
	        int[] weights = {6,5,7,2,3,4,5,6,7};
	        int j = 0, sum = 0, control = 0;
	        int csum = new Integer(nip.substring(nsize - 1)).intValue();
	        if (csum == 0) {
	            csum = 10;
	        }
	        for (int i = 0; i < nsize - 1; i++) {
	            char c = nip.charAt(i);
	            j = new Integer(String.valueOf(c)).intValue();
	            sum += j * weights[i];
	        }
	        control = sum % 11;
	        return (control == csum);
	    }

	public CheckResult checkRule(Person entity) {
		if(entity.getNip()==null)
			return new CheckResult("", RuleResult.Error);
		if(entity.getNip().equals(""))
			return new CheckResult("", RuleResult.Error);
		if (!isValidNip(entity.getNip()))
			return new CheckResult("", RuleResult.Error);
		return new CheckResult("", RuleResult.Ok);
		
	}

}
