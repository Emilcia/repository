package main;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import repositories.impl.RepositoryCatalog;
import unitofwork.IUnitOfWork;
import unitofwork.UnitOfWork;
import domain.Person;
import domain.User;

public class Main {

	private static SessionFactory buildSessionFactory() {
		try {

			return new Configuration().configure().buildSessionFactory();
				
		} catch (Throwable ex) {
		
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
	public static void main(String[] args) throws Exception {
	    
		SessionFactory session = buildSessionFactory();
		session.openSession();
		session.getCurrentSession().beginTransaction();
		
		
		Person person = new Person();
		person.setFirstName("Jan");
		
		
		User mkonopko = new User();
		mkonopko.setLogin("mariakonopko");
		mkonopko.setPassword("maryska11");
		mkonopko.setPerson(person);
		
		IUnitOfWork uow = new UnitOfWork(session);
			
		RepositoryCatalog catalog = new RepositoryCatalog(session, uow);
		
			
			
		catalog.getUsers().save(mkonopko);
		catalog.commit();
		
		
		List<User> usersFromDb= catalog.getUsers().getAll();
		
		for(User userFromDb: usersFromDb)
			System.out.println(userFromDb.getId()+" "+userFromDb.getLogin()+" "+userFromDb.getPassword());
		
		User u = catalog.getUsers().get(1);
		u.setPassword("111");
		catalog.getUsers().update(u);

		catalog.commit();
		
		//catalog.getUsers().delete(usersFromDb.get(0));
		//uow.commit();
		//for(User userFromDb: catalog.getUsers().getAll())
		//	System.out.println(userFromDb.getId()+" "+userFromDb.getLogin()+" "+userFromDb.getPassword());
		
		
		

}

}
