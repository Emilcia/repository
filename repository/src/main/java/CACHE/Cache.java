package CACHE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Cache<TEntity> {

	Map<String, ArrayList<TEntity>> cacheObjects = new HashMap<String, ArrayList<TEntity>>();
	
	public void Clean()
	{
		for(String x : cacheObjects.keySet())
		{
			cacheObjects.remove(x);
		}
	}
	private Cache(){}
	
	private static Cache singl;
	private static Object token = new Object();
	
	public static Cache getInstance()
	{
		if(singl == null){
			synchronized(token)
			{
				if(singl==null)
					singl = new Cache();
			}
		}
		return singl;
	}
	public void put(String Classname,List<TEntity> list){
		if (!getObjects().containsKey(Classname)){
			ArrayList<TEntity> objects = new ArrayList<TEntity>();
			getObjects().put(Classname, objects);
		}
		
		getObjects().get(Classname).addAll(list);
	}
	
	public ArrayList<TEntity> get(String key){
		return new ArrayList<TEntity> (getObjects().get(key));
	}

	public Map <String, ArrayList<TEntity> > getObjects() {
		return cacheObjects;
	}

}
