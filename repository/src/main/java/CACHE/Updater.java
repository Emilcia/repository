package CACHE;

import repositories.impl.RepositoryCatalog;

public class Updater implements Runnable {
	
	private int lifespan;
	
	Cache cache = Cache.getInstance();

	public boolean isRunning = true;
	private RepositoryCatalog catalog;
	
	public Updater(RepositoryCatalog catalog){
		this.catalog=catalog;
	}
	
	public synchronized void run() {
		
		isRunning = true;
		System.out.println("Updated is running.");
		
		try {
				for(int i = 1;i<=2;i++)
				{
					Thread.sleep(lifespan*1000);
					update(catalog);
					System.out.println("Updated cache!");
				}
						
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		isRunning=false;
		System.out.println("Updater finished.");
	}
	
	
	public void update(RepositoryCatalog catalog){
		
		 cache.Clean();
		 cache.put("Users", catalog.getUsers().getAll());
		 cache.put("Persons",catalog.getPersons().getAll());
		 //      ...  inne tabele w bazie
	 }
	

	
	public int getLifespan() {
		return lifespan;
	}

	public void setLifespan(int lifespan) {
		this.lifespan = lifespan;
	}

}
