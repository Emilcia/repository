package repositories.impl;


import java.sql.SQLException;
import java.sql.Date;
import java.util.List;

import org.hibernate.SessionFactory;

import domain.Entity;
import domain.Person;
import unitofwork.IUnitOfWork;

public class PersonRepository 
	extends Repository<Person>{
	

	protected PersonRepository(SessionFactory session,
			IEntityBuilder<Person> builder, IUnitOfWork uow) {
		super(session, builder, uow);
	}

	@Override
	protected void setUpUpdateQuery(Person entity) throws SQLException {
		update.setString(1, entity.getFirstName());
		update.setString(2, entity.getSurname());
		update.setString(3, entity.getPesel());
		update.setString(4, entity.getNip());
		update.setDate(5, (Date) entity.getDate());
		update.setString(6, entity.getEmail());
		update.setInt(7, entity.getId());
		
	}

	@Override
	protected void setUpInsertQuery(Person entity) throws SQLException {
		insert.setString(1, entity.getFirstName());
		insert.setString(2, entity.getSurname());
		insert.setString(3, entity.getPesel());
		insert.setString(4, entity.getNip());
		insert.setDate(5, (Date) entity.getDate());
		insert.setString(6, entity.getEmail());
	}

	@Override
	protected String getTableName() {
		return "person";
	}

	@Override
	protected String getUpdateQuery() {
		return "update person set (name,surname,pesel,nip,date)=(?,?,?,?,?,?)"
				+ "where id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "insert into person(name,surname,pesel,nip,date) values(?,?,?,?,?,?)";
	}

	public Person get(int id) {
		
		return session.getCurrentSession().get(Person.class, id);
	}

	public List<Person> getAll() {
		
		return session.getCurrentSession().createQuery("SELECT p FROM Person p").list();
	}
	
}
