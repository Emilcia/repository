package repositories.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;

import repositories.IRepository;
import unitofwork.IUnitOfWork;
import unitofwork.IUnitOfWorkRepository;
import domain.Entity;

public abstract class Repository<TEntity extends Entity> 
	implements IRepository<TEntity>, IUnitOfWorkRepository
{
	
	protected IUnitOfWork uow;
	protected SessionFactory session;
	protected PreparedStatement selectByID;
	protected PreparedStatement insert;
	protected PreparedStatement delete;
	protected PreparedStatement update;
	protected PreparedStatement selectAll;
	protected IEntityBuilder<TEntity> builder;

	protected String selectByIDSql=
			"SELECT * FROM "
			+ getTableName()
			+ " WHERE id=?";
	protected String deleteSql=
			"DELETE FROM "
			+ getTableName()
			+ " WHERE id=?";
	protected String selectAllSql=
			"SELECT * FROM " + getTableName();
	
	protected Repository(SessionFactory session,
			IEntityBuilder<TEntity> builder, IUnitOfWork uow)
	{
		this.uow=uow;
		this.builder=builder;
		this.session = session;

	}
	
	public void save(TEntity entity)
	{
		uow.markAsNew(entity, this);
	}

	
	public void update(TEntity entity)
	{
		uow.markAsDirty(entity, this);
	}

	
	public void delete(TEntity entity)
	{
		uow.markAsDeleted(entity, this);
	}

	
	public void persistAdd(Entity entity) {
			
		session.getCurrentSession().save(entity);
		
	}


	
	public void persistUpdate(Entity entity) {

		session.getCurrentSession().update(entity);
		
	}

	
	public void persistDelete(Entity entity) {

		session.getCurrentSession().delete(entity);
		
	}

	
	protected abstract void setUpUpdateQuery(TEntity entity) throws SQLException;
	protected abstract void setUpInsertQuery(TEntity entity) throws SQLException;
	protected abstract String getTableName();
	protected abstract String getUpdateQuery();
	protected abstract String getInsertQuery();
}
