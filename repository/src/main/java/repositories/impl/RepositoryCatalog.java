package repositories.impl;


import org.hibernate.SessionFactory;

import domain.Person;
import domain.Role;
import repositories.IRepository;
import repositories.IRepositoryCatalog;
import repositories.IUserRepository;
import unitofwork.IUnitOfWork;

public class RepositoryCatalog implements IRepositoryCatalog{

	private SessionFactory session;
	private IUnitOfWork uow;
	
	public RepositoryCatalog(SessionFactory session, IUnitOfWork uow) {
		super();
		this.session = session;
		this.uow = uow;
	}

	
	public IUserRepository getUsers() {
		return new UserRepository(session, new UserBuilder(), uow);
	}

	
	public IRepository<Person> getPersons() {
		return new PersonRepository(session, new PersonBuilder(), uow);
	}

	
	public IRepository<Role> getRoles() {
		return null;
	}

	
	public void commit() {
		uow.commit();
	}

}
