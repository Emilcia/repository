package repositories.impl;


import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import repositories.IRepositoryCatalog;
import unitofwork.IUnitOfWork;
import unitofwork.UnitOfWork;

public class RepositoryCatalogProvider {

	private static SessionFactory buildSessionFactory() {
		try {

			return new Configuration().configure().buildSessionFactory();
				
		} catch (Throwable ex) {
		
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
	public static IRepositoryCatalog catalog()
	{

		SessionFactory sessionFactory = buildSessionFactory();
		
		IUnitOfWork uow = new UnitOfWork(sessionFactory);
		IRepositoryCatalog catalog = new RepositoryCatalog(sessionFactory, uow);
		
		return catalog;
		
	}
	
}
