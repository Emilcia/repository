package domain;

import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name = "roles")
public class Role extends Entity {

	private String name;
	
	private List<Privilege> privileges;
	private List<User> users;
	
	public Role()
	{
		privileges=new ArrayList<Privilege>();
		users= new ArrayList<User>();
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@ElementCollection(targetClass=Privilege.class)
	public List<Privilege> getPrivileges() {
		return privileges;
	}
	public void setPrivileges(List<Privilege> privileges) {
		this.privileges = privileges;
	}
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@ElementCollection(targetClass=User.class)
	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	
}