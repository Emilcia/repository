package domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Table;


@javax.persistence.Entity
@Table(name = "privilages")
public class Privilege extends Entity {

	private String name;
	
	private List<Role> roles;

	public Privilege()
	{
		roles = new ArrayList<Role>();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@ElementCollection(targetClass=Role.class)
	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	
}
