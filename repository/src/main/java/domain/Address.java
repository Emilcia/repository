package domain;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name="address")
public class Address extends Entity {

	private String country;
	private String city;
	private String postalCode;
	private String street;
	private String houseNumber;
	private String localNumber;
	private EnumerationValue type;
	
	private Person person;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	public String getLocalNumber() {
		return localNumber;
	}
	public void setLocalNumber(String localNumber) {
		this.localNumber = localNumber;
	}
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public EnumerationValue getType() {
		return type;
	}
	public void setType(EnumerationValue type) {
		this.type = type;
	}
	
}