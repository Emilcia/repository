package test;

import static org.junit.Assert.*;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.PeselRule;
import domain.Person;

public class PeselRuleTest {

	PeselRule rule = new PeselRule();
	
	@Test
	public void checker_should_check_if_the_person_pesel_is_not_null(){
		Person p = new Person();
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_check_if_the_person_pesel_is_not_empty(){
		Person p = new Person();
		p.setPesel("");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_return_OK_if_the_pesel_is_correct(){
		Person p = new Person();
		p.setPesel("94032002545");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}
	}
