package test;

import static org.junit.Assert.*;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.EmailRule;
import domain.Person;

public class EmailRuleTest {

	EmailRule rule = new EmailRule();
	
	@Test
	public void checker_should_check_if_the_person_email_is_not_null(){
		Person p = new Person();
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_check_if_the_person_email_is_not_empty(){
		Person p = new Person();
		p.setEmail("");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_return_OK_if_the_email_is_correct(){
		Person p = new Person();
		p.setEmail("m@wp.pl");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}
	}