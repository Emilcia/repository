package test;

import static org.junit.Assert.*;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.NipRule;
import domain.Person;

public class NipRuleTest {

	NipRule rule = new NipRule();
	
	@Test
	public void checker_should_check_if_the_person_nip_is_not_null(){
		Person p = new Person();
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_check_if_the_person_nip_is_not_empty(){
		Person p = new Person();
		p.setNip("");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void checker_should_return_OK_if_the_nip_is_correct(){
		Person p = new Person();
		p.setNip("123-456-32-18");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}
	
	//...reszta testów

}